export default {
  async fetchArtists({ commit }) {
    const { data } = await this.$axios.get('/api/artists')
    commit('setArtists', data)
  },
  async fetchArtist({ commit }, id) {
    const { data } = await this.$axios.get(`/api/artists/${ id }`)
    commit('setArtist', data)
  },
  async addArtist({ commit, dispatch }, name) {
    const { data } = await this.$axios.post('/api/artists', {
      name
    })
    commit('setArtist', data)
    dispatch('fetchArtists')
  },
  async editArtist({ commit }, artist) {
    const { data } = await this.$axios.post(`/api/artists/${ artist.id }`, {
      name: artist.name
    })
    commit('setArtist', data)
  }
}
