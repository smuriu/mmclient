import { set } from '@/utils/vuex'

export default {
  setArtists: set('artists'),
  setArtist: set('artist')
}
